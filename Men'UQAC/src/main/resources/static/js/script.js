function showSubSections(parent) {
	var all = Array.prototype.slice.call(document.getElementsByClassName("parent"));
	all.forEach(element => element.style.display = "none");
	var sub = Array.prototype.slice.call(document.getElementsByClassName(parent));
	sub.forEach(element => element.style.display = "block");
}

function minusQuantity(proto) {
	console.log(proto);
	var element = document.getElementById(proto);
	element.value = element.value.split('£')[0];
	var elementView = document.getElementById("View" + proto);
	elementView.textContent = parseInt(elementView.textContent) - 1;
}

function AddQuantity(proto, sauces, cookings) {
	//var element = document.getElementById(proto);
	//element.value = parseInt(element.value) + 1;
	var elementView = document.getElementById("View" + proto);
	elementView.textContent = parseInt(elementView.textContent) + 1;

	var actualOrder = document.getElementById("actualOrder");
	var orders = actualOrder.innerHTML;
	var index = actualOrder.getElementsByTagName("div").length + 1;
	var s = `<div id="proto` + index + `">
				<a>` + proto + `</a>
				<button class="ModificationButton" type="button" onclick='deleteProto(` + index + `, "` + proto + `")'>
					Delete
				</button><button class="ModificationButton" type="button" onclick='displayModify(` + index + `)'>
					Modify
				</button>
				<div id="modify` + index + `" style="display:none;">
					<div class="WithIce">
						<label>Ice Cubes ?</label>
						<input name="ice` + index + `" value="yes" type="radio" checked>With Ice</input>
						<input name="ice` + index + `" value="no" type="radio">Without Ice</input>
						</br>
					</div>
					<div class="WithSugar">
						<label>Sugar ?</label>
						<input name="sugar` + index + `" value="yes" type="radio" checked>With Sugar</input>
						<input name="sugar` + index + `" value="no" type="radio">Without Sugar</input>
						</br>
					</div>
					<div class="WichSauce">
						<label>Which sauce ?</label>
						<div>`;
	sauces.forEach(element => s = s.concat(`<input name="sauce` + index + `" value="`+ element + `" type="radio">`, element, `</input>`));
	s = s.concat(`</br></div></div><div class="WichCooking"><label>Which cooking ?</label>`);
	cookings.forEach(element => s = s.concat(`<input name="cooking` + index + `" value="`+ element + `" type="radio">`, element, `</input>`));
	s = s.concat(`</br></div>
				<div class="Comment">
					<label>Any comments ?</label>
					<textarea id="comment` + index + `" name="comments` + index + `" placeholder="any comments like : without oignons"></textarea>
					</br>
				</div>
				<button type="button" class="ModificationButton" onclick="sendModification('` + proto + `', ` + index + `)">Ok</button>
				</div>
				</br>
				</div>`);
	orders = orders.concat(s);
	actualOrder.innerHTML = orders;
	sendModification(proto, index);
}

//Envoi des listes de modifications : ["parent; proto | id§ ice§ sugar§ sauce§ cooking§ comment | id§ ice§ sugar§ sauce§ cooking§ comment", ...]
function sendModification(proto, index){
	hideModify(index);
	var element = document.getElementById(proto);
	var s = element.value;
	s = s.concat("£");
	s = s.concat(index);
	s = s.concat("§");
	let ices = document.querySelectorAll('input[name="ice'+index+'"]');
	var b = false;
	for (const ice of ices) {
        if (ice.checked) {
            s = s.concat(ice.value, "§");
            b = true;
            break;
        }
    }
	if(!b){
		s = s.concat("no§");
	}
	b = false;
	let sugars = document.querySelectorAll('input[name="sugar'+index+'"]');
	for (const sugar of sugars) {
        if (sugar.checked) {
        	s = s.concat(sugar.value, "§");
        	b = true;
            break;
        }
    }
	if(!b){
		s = s.concat("no§");
	}
	b = false;
	let sauces = document.querySelectorAll('input[name="sauce'+index+'"]');
	for (const sauce of sauces) {
        if (sauce.checked) {
        	s = s.concat(sauce.value, "§");
        	b = true;
            break;
        }
    }
	if(!b){
		s = s.concat("NONE§");
	}
	b = false;
	let cookings = document.querySelectorAll('input[name="cooking'+index+'"]');
	for (const cooking of cookings) {
        if (cooking.checked) {
        	s = s.concat(cooking.value, "§");
        	b = true;
            break;
        }
    }
	if(!b){
		s = s.concat("MR§");
	}
	b = false;
	let comment = document.getElementById("comment"+index);
	s = s.concat(comment.value);
	s = s.concat(" ");
	element.value = s;
}

function deleteProto(index, proto) {
	document.getElementById("proto" + index).innerHTML = "";
	minusQuantity(proto);
}

function displayModify(index){
	document.getElementById("modify" + index).style.display = "block";
}

function hideModify(index){
	document.getElementById("modify" + index).style.display = "none";
}