package menuqac.prototype;

import menuqac.Prototype;
import menuqac.Visitor;
import menuqac.visitor.SauceVisitor;
import menuqac.visitor.SauceVisitor.Sauce;

public class Food extends Prototype {
	private static final long serialVersionUID = 1L;

	protected Sauce sauce;

	public Food(String _name, double _price, int _target, String _comment) {
		super(_name, _price, _target, _comment);
		this.sauce = Sauce.NONE;
	}

	public void setSauce(Sauce sauce) {
		this.sauce = sauce;
	}

	public Sauce getSauce() {
		return this.sauce;
	}

	@Override
	public Prototype clone() {
		Food m = new Food(this.name, this.price.doubleValue(), this.target, this.comment);
		m.setSauce(this.sauce);
		return m;
	}

	@Override
	public void accept(Visitor v, Object value) {
		if (v instanceof SauceVisitor) {
			this.setSauce((Sauce) value);
		}
	}

	@Override
	public String toString() {
		String s = "<p style=\"text-align:left; color: black; display:inline-block; padding-top:3%; width:70%; font-weight:bold;\">" + this.name + "<p style = \"display: inline-block; text-align:right; color: black; width: 30%;\">"
				+ this.price + " &euro;</p><p style=\"display : block; margin: 1%;\">"
				+ (this.sauce == Sauce.NONE ? "" : "<p style=\"text-indent: 10%; display : block; margin: 1%;\">" + this.sauce + "</p>")
				+ (this.comment == null ? "" : "<p style=\"text-indent: 10%; display : block; margin: 1%;\">" + this.comment + "</p>");
		return s;
	}
}