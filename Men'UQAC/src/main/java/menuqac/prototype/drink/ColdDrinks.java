package menuqac.prototype.drink;

import menuqac.Prototype;
import menuqac.Visitor;
import menuqac.prototype.Drinks;
import menuqac.visitor.IceCubesVisitor;

public class ColdDrinks extends Drinks {
	private static final long serialVersionUID = 1L;

	private boolean hasIceCubes;

	public ColdDrinks(String _name, double _price, int _target, String _comment) {
		super(_name, _price, _target, _comment);
		this.hasIceCubes = true;
	}

	public void setIceCubes(boolean hasIceCubes) {
		this.hasIceCubes = hasIceCubes;
	}

	public boolean getIceCubes() {
		return this.hasIceCubes;
	}

	@Override
	public void accept(Visitor v, Object value) {
		if (v instanceof IceCubesVisitor) {
			this.setIceCubes((boolean) value);
		}
	}

	@Override
	public Prototype clone() {
		ColdDrinks cd = new ColdDrinks(this.name, this.price.doubleValue(), this.target, this.comment);
		cd.setIceCubes(this.hasIceCubes);
		return cd;
	}

	@Override
	public String toString() {
		String s = "<p style=\"text-align:left; color: black; display:inline-block; padding-top:3%; width:70%; font-weight:bold;\">" + this.name + "<p style = \"display: inline-block; text-align:right; color: black; width: 30%;\">"
				+ this.price + " &euro;</p><p style=\"text-indent: 10%; display : block; margin: 1%;\">"
				+ (this.hasIceCubes ? "w icecubes" : "w/o icecubes") + "</p>"
				+ (this.comment == null ? "" : "<p style=\"text-indent: 10%; display : block; margin: 1%;\">" + this.comment + "</p>");
		return s;
	}
}