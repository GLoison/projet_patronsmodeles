package menuqac.prototype.drink;

import menuqac.Prototype;
import menuqac.Visitor;
import menuqac.prototype.Drinks;
import menuqac.visitor.SugarVisitor;

public class HotDrinks extends Drinks {
	private static final long serialVersionUID = 1L;

	private boolean hasSugar;

	public HotDrinks(String _name, double _price, int _target, String _comment) {
		super(_name, _price, _target, _comment);
		this.hasSugar = true;
	}

	public void setSugar(boolean hasSugar) {
		this.hasSugar = hasSugar;
	}

	public boolean getSugar() {
		return this.hasSugar;
	}

	@Override
	public void accept(Visitor v, Object value) {
		if (v instanceof SugarVisitor) {
			this.setSugar((boolean) value);
		}
	}

	@Override
	public Prototype clone() {
		HotDrinks hd = new HotDrinks(this.name, this.price.doubleValue(), this.target, this.comment);
		hd.setSugar(this.hasSugar);
		return hd;
	}

	@Override
	public String toString() {
		String s = "<p style=\"text-align:left; color: black; display:inline-block; padding-top:3%; width:70%; font-weight:bold;\">" + this.name + "<p style = \"display: inline-block; text-align:right; color: black; width: 30%;\">"
				+ this.price + " &euro;</p><p style=\"text-indent: 10%; display : block; margin: 1%;\">"
				+ (this.hasSugar ? "w sugar" : "w/o sugar") + "</p>"
				+ (this.comment == null ? "" : "<p  style=\"text-indent: 10%; display : block; margin: 1%;\">" + this.comment + "</p>");
		return s;
	}
}