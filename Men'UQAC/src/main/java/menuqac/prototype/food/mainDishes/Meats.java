package menuqac.prototype.food.mainDishes;

import menuqac.Prototype;
import menuqac.Visitor;
import menuqac.prototype.food.MainDishes;
import menuqac.visitor.CookingVisitor;
import menuqac.visitor.SauceVisitor;
import menuqac.visitor.CookingVisitor.Cooking;
import menuqac.visitor.SauceVisitor.Sauce;

public class Meats extends MainDishes {
	private static final long serialVersionUID = 1L;

	private Cooking cooking;

	public Meats(String _name, double _price, int _target, String _comment) {
		super(_name, _price, _target, _comment);
		this.cooking = Cooking.MR;
	}

	public void setCooking(Cooking cooking) {
		this.cooking = cooking;
	}

	public Cooking getCooking() {
		return this.cooking;
	}

	@Override
	public void accept(Visitor v, Object value) {
		if (v instanceof SauceVisitor) {
			this.setSauce((Sauce) value);
		} else if (v instanceof CookingVisitor) {
			this.setCooking((Cooking) value);
		}
	}

	@Override
	public Prototype clone() {
		Meats m = new Meats(this.name, this.price.doubleValue(), this.target, this.comment);
		m.setCooking(this.cooking);
		m.setSauce(this.sauce);
		return m;
	}

	@Override
	public String toString() {
		String s = "<p style=\"text-align:left; color: black; display:inline-block; padding-top:3%; width:70%; font-weight:bold;\">" + this.name 
				+ "<p style = \"display: inline-block; text-align:right; color: black; width: 30%;\">"
				+ this.price + " &euro;</p><p style=\"display : block; margin: 1%;\"><p  style=\"text-indent: 10%; display : block; margin: 1%;\">" 
				+ this.cooking
				+ (this.sauce == Sauce.NONE ? "" : " " + this.sauce) + "</p>"
				+ (this.comment == null ? "" : "<p style=\"text-indent: 10%; display : block; margin: 1%;\">" + this.comment + "</p>");
		return s;
	}
}