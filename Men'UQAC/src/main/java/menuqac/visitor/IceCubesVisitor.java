package menuqac.visitor;

import menuqac.Prototype;
import menuqac.Visitor;

public class IceCubesVisitor implements Visitor {
	@Override
	public void visit(Prototype p, Object value) {
		p.accepts(this, (boolean) value);
	}
}