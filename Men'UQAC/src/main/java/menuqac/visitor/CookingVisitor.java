package menuqac.visitor;

import menuqac.Prototype;
import menuqac.Visitor;

public class CookingVisitor implements Visitor {
	public enum Cooking {
		VR, R, MR, WD // VERY_RARE, RARE, MEDIUM_RARE, WELL_DONE
	}

	@Override
	public void visit(Prototype p, Object value) {
		p.accepts(this, (Cooking) value);
	}
}