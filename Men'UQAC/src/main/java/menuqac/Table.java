package menuqac;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class Table implements Serializable {
	private static final long serialVersionUID = 1L;

	private int number;
	private List<Order> orders;
	private BigDecimal price;

	public Table(int number) {
		this.number = number;
		this.orders = new LinkedList<Order>();
		this.price = BigDecimal.valueOf(0);
	}

	public double getPrice() {
		return this.price.doubleValue();
	}
	
	public void decreasePrice(BigDecimal p) {
		this.price = this.price.subtract(p);
	}

	public List<Prototype> getPrototypes() {
		LinkedList<Prototype> result = new LinkedList<Prototype>();
		for (Order o : orders) {
			result.addAll(o.get());
		}
		return result;
	}

	public int getNumber() {
		return this.number;
	}

	public void addOrder(Order o) {
		this.orders.add(o);
		this.price = this.price.add(o.getPrice());
	}

	public void flush() {
		this.orders.clear();
	}

	@Override
	public String toString() {
		return ("Table " + this.number);
	}
}