package menuqac;
//package com.tutorialspoint.demo.controller;

import java.math.BigDecimal;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import menuqac.app.Application;
import menuqac.app.Waiter;

@Controller
public class WebController {
	
	private Application app = new Application();
	private Table selectedTable = this.app.getTables().isEmpty() ? null : this.app.getTables().get(0);
	
   @RequestMapping(value = "/index")
   public ModelAndView index() {
	   ModelAndView mav = new ModelAndView("index");
       return mav;
   }
   
   @RequestMapping(value="/cashRegister")
   public ModelAndView cashRegister() {
	   ModelAndView mav = new ModelAndView("cashRegister");
	   mav.addObject("tables", app.getTables());
	   mav.addObject("selectedTable", this.selectedTable);
       return mav;
   }
   
   @RequestMapping(value="/tableChoice")
   public String cashRegisterTableChoice(@RequestParam(required=false) int table) {
	   this.selectedTable = this.app.findTable(table);
       return "redirect:/cashRegister";
   }
   
   @RequestMapping(value="/payment")
   public String cashRegisterTableChoice(@RequestParam(required=false) double somme) {
	   this.selectedTable.decreasePrice(BigDecimal.valueOf(somme));
       return "redirect:/cashRegister";
   }
   
   @RequestMapping(value="/flushTable")
   public String cashRegisterFlushTable() {
	   this.selectedTable.flush();
       return "redirect:/cashRegister";
   }
   
   @RequestMapping(value="/waiter")
   public ModelAndView waiter(@RequestParam(required=false) int id) {
	   Waiter w = app.getWaiter(id);
	   if(w != null) {
		   ModelAndView mav = new ModelAndView("waiter");
	       mav.addObject("waiter", app.getWaiter(id));
	       return mav;
	   }
	   return new ModelAndView("indexBadWaiter");
   }
   
   @RequestMapping(value="/Addwaiter")
   public String Addwaiter(@RequestParam(required=false) int id) {
	   this.app.AddWaiter(id);
	   return "redirect:/index";
   }
   
   @RequestMapping(value="/commande")
   public String order(@RequestParam(required=false) int waiter, @RequestParam(required=false) int table, @RequestParam(required=false) String[] order) {
	   this.app.createOrder(this.app.findWaiter(waiter), table, order);
       return "redirect:/waiter?id="+waiter;
   }
}