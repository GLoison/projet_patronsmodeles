package menuqac.app;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import menuqac.Menu;
import menuqac.Order;
import menuqac.Table;
import menuqac.visitor.CookingVisitor.Cooking;
import menuqac.visitor.SauceVisitor;
import menuqac.visitor.SauceVisitor.Sauce;

public class Waiter implements Serializable {
	private static final long serialVersionUID = 1L;

	private Order order;

	private int number;
	private Menu menu;

	private ArrayList<Table> tables;


	public Waiter(int number, ArrayList<Table> tables, Menu menu) {
		this.setNumber(number);
		this.tables = tables;
		this.order = new Order(1, number, 1);
		this.menu = menu;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public void emptyTables() {
		this.getTables().clear();
	}

	public void addTables(Table table) {
		this.getTables().add(table);
	}
	
	public void removeTables(Table table) {
		this.getTables().remove(table);
	}

	public ArrayList<Table> getTables() {
		return tables;
	}
	
	public ArrayList<String> getPossibleSauce() {
		ArrayList<String> result = new ArrayList<String>();
		EnumSet<Sauce> enumSet = EnumSet.allOf(Sauce.class) ;
		for(Sauce s : enumSet) {
			result.add("\"" + s.toString() + "\"");
		}
		return result;
	}
	
	public ArrayList<String> getPossibleCooking() {
		ArrayList<String> result = new ArrayList<String>();
		EnumSet<Cooking> enumSet = EnumSet.allOf(Cooking.class) ;
		for(Cooking s : enumSet) {
			result.add("\"" + s.toString() + "\"");
		}
		return result;
	}
}