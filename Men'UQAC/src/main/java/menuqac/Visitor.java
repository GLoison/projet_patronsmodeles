package menuqac;

public interface Visitor {
	public void visit(Prototype p, Object o);
}